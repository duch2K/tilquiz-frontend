import React, { Dispatch, FC, SetStateAction } from 'react';
import cn from 'classnames';
import styles from './Sidebar.module.scss';
import { useRouter } from 'next/router';

interface Props {
  lessons: any[];
  currLesson: any;
  setLesson: Dispatch<SetStateAction<any>>;
  setIsOpen?: Dispatch<SetStateAction<any>>;
}

export const Sidebar: FC<Props> = ({lessons, currLesson, setLesson, setIsOpen }) => {
  const router = useRouter();

  const itemClick = (item: any) => {
    setLesson(item);

    if (setIsOpen) {
      setIsOpen(false);
    }

    // router.push('/lessons/' + item.id + '/group/' + item.group);
    router.push('/lessons/' + item.id + '/group/1');
  };

  // const sortedLessons = lessons.filter(item => itemitem.group === router.query.groupId);
  const sortedLessons = lessons;

  return (
    <div className={cn(styles.sidebar, 'pt-3 px-3')}>
      {sortedLessons.sort((a, b) => a.id - b.id).map((item, idx) => (
        <div
          key={item.id}
          className={cn(styles.item, 'py-2', {
            [styles.active]: currLesson && currLesson.id === item.id
          })}
          onClick={() => itemClick(item)}
        >
          №{idx+1} сабақ
          <br />
          {item.name}
        </div>
      ))}
    </div>
  );
};
