import styles from './Task.module.scss';

export const Task = () => {
  return (
    <div className={styles.task}>
      <h5>Задание:</h5>
      <div className={styles.file}>
        <a
          href="tilquiz.kz:7000/lessons/static"
          download="tilquiz.kz:7000/lessons/static"
        >Задание_1.docx</a>
      </div>
    </div>
  );
};
