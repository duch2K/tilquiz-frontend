import { Container, Button } from 'react-bootstrap';
import styles from './LessonHead.module.scss';
import React from 'react';

//@ts-ignore
export const LessonHead = ({ setIsOpen }) => {

  return (
    <div className={styles.lessonHead} id="lesson-head">
      <Container className={styles.container}>
        <Button className={styles.burgerBtn} variant="light" onClick={() => setIsOpen(true)}>
          <img src="/images/burger-menu.png" />
        </Button>
      </Container>
    </div>
  )
}
