import React, { FC, useCallback, useState } from 'react';
import Dropzone from 'react-dropzone';
import { Button } from 'react-bootstrap';
import { parseCookies } from 'nookies';
import axios from 'axios';
import cn from 'classnames';
import styles from '@/components/lessonsPage/LessonComponent/LessonComponent.module.scss';
import { apiUrl } from '@/config';

interface Props {
  uploaded: any;
  current: any;
  getUploaded?: any;
}

export const Upload: FC<Props> = ({ uploaded, current, getUploaded }) => {
  const cookies = parseCookies(null);
  const [files, setFiles] = useState<any[]>([]);
  const [editMode, setEditMode] = useState<boolean>(false);

  console.log('upload', uploaded);

  const onDrop = useCallback((uploadedFiles) => {
    setFiles(uploadedFiles);
  }, []);

  const upload = async () => {
    const formData = new FormData();
    formData.append('homework', files[0]);

    const res: any = await axios.post(apiUrl + '/lessons/uploadHomework', formData,
      {
        params: {
          userid: JSON.parse(cookies.auth).id,
          lessonid: current.id
        },
        headers: {
          'Content-Type': 'multipart/form-data'
        }
      }
    );

    // setFiles([]);
    getUploaded();
  };

  const cancel = () => {
    if (editMode) setEditMode(false);
    if (files.length) setFiles([]);
  };

  return (
    <>
      {uploaded && !editMode ? (
        <>
          <div className={cn(styles.fileDrop, { fileDrop_edit: editMode })}>
            <a
              key={uploaded.id}
              href={apiUrl + '/lessons/download/' + uploaded.homework}
              download={apiUrl + '/lessons/download/' + uploaded.homework}
            >
              {uploaded.homework}
            </a>
          </div>
          <div className={styles.uploadBtns}>
            <Button variant="info" onClick={() => setEditMode(true)}>Редактировать</Button>
          </div>
        </>
      ) : (
        <Dropzone onDrop={onDrop}>
          {({getRootProps, getInputProps}) => (
            <>
              <div className={styles.fileDrop} style={{borderStyle: editMode ? 'dashed' : 'solid'}}>
                <div {...getRootProps()}>
                  <input {...getInputProps()} />
                  {!files.length ? (
                    <>
                      <b>Загрузите файл с заданием</b>
                      <span>{uploaded && uploaded.homework}</span>
                    </>
                  ) : (
                    <div>
                      {files.map(item => (
                        <span key={item.name}>{item.name}</span>
                      ))}
                    </div>
                  )}
                </div>
              </div>
              {editMode && !files.length ? (
                <div className={styles.uploadBtns}>
                  <Button size="sm" variant="danger" onClick={cancel}>Отмена</Button>
                </div>
              ) : files.length ? (
                <div className={styles.uploadBtns}>
                  <Button size="sm" variant="info" onClick={upload}>Загрузить</Button>
                  <Button size="sm" variant="danger" onClick={cancel}>Отмена</Button>
                </div>
              ) : null}
            </>
          )}
        </Dropzone>
      )}
    </>
  );
};
