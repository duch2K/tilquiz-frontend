import React, { FC, useEffect, useState } from 'react';
import { Button } from 'react-bootstrap';
import { parseCookies } from 'nookies';
import { apiUrl } from '@/config';
import styles from './LessonComponent.module.scss';
import Link from 'next/link';
import { Upload } from '@/components/lessonsPage/Upload/Upload';
import { Task } from '@/components/lessonsPage/Task/Task';

interface Props {
  lessons: any[];
  current: any;
}

export const LessonComponent: FC<Props> = ({ lessons, current }) => {
  const [uploaded, setUploaded] = useState<any>(null);
  const cookies = parseCookies(null);

  useEffect(() => {
    getUploaded();
  }, [current]);

  const getUploaded = async () => {
    if (current) {
      console.log('YO', cookies.auth)
      const user = current.users.find((item: any) =>
        item.id == JSON.parse(cookies.auth).id
      );

      setUploaded(user?.UserLessons);
    }
  };

  return (
    <div className={styles.lesson}>
      {lessons.length ? (
        <>
          <h3>№{current?.id} сабақ. {current?.name}</h3>
          <div className={styles.lessonContent}>
            <video src={`${apiUrl}/lessons/static/${current?.video_link}`} controls />
            <p>{ current?.text }</p>
          </div>
        </>
      ) : (
        <h2 className="text-muted">Нет уроков</h2>
      )}

      <hr/>
      <Task />
      <Upload uploaded={uploaded} current={current} getUploaded={getUploaded} />
      <hr/>
      {current  && (
        <Link href={`/lessons/${current.id}/test`}>
          <a>
            <Button className={styles.quizBtn}>Сдать тест</Button>
          </a>
        </Link>
      )}
    </div>
  );
};
