import React, { FC, useEffect, useState } from 'react';
import Link from 'next/link';
import { Container, Nav, Navbar, Button, Dropdown, DropdownButton } from 'react-bootstrap';
import { useRouter } from 'next/router';
import { destroyCookie, parseCookies } from 'nookies';
import cn from 'classnames';
import axios from 'axios';
import styles from './Header.module.scss';
import { apiUrl } from '@/config';

interface User {
  id: number;
  name: string;
  surname: string;
  email: string;
}

export const Header: FC = () => {
  const router = useRouter();
  const cookies = parseCookies(null);
  const [user, setUser] = useState<User | null>(null);

  useEffect( () => {
    if (cookies.auth) {
      getUser();
    }
  }, []);

  const getUser = async () => {
    let auth = null;
    if (cookies.auth) {
      auth = JSON.parse(cookies.auth);
    }
    const { data } = await axios.get(apiUrl + '/users/' + auth.id);
    setUser(data);
  };

  const logout = () => {
    destroyCookie(null, 'auth');
    router.push('/login');
  };


  return (
    <header className={styles.header} id="header">
      <Navbar className={styles.navbar} bg="light" expand="lg">
        <Container>
          <Navbar.Brand className={styles.navbarBrand}>
            <Link href="/">
              <a>
                <img src="/images/logo.png" width={50} alt="logo" />
              </a>
            </Link>
          </Navbar.Brand>

          <Nav
            className={cn(styles.nav, 'me-auto my-2 my-lg-0')}
            navbarScroll
          >
            {router.pathname === '/' ? (
              <>
                <Nav.Link href="#about">О нас</Nav.Link>
                <Nav.Link href="#cost">Обучение</Nav.Link>
                <Nav.Link href="#footer">Контакты</Nav.Link>
              </>
            ) : (
              <Nav.Link href="/">Главная</Nav.Link>
            )}
          </Nav>

          <div style={{display: 'flex', gap: 20}}>
            <Link href="/trial-test">
              <Button variant="primary">Сдать пробный тест</Button>
            </Link>
            {cookies.auth ? (
              <DropdownButton
                variant="outline"
                size="lg"
                title={user?.name + ' ' + user?.surname}
              >
                <Dropdown.Item>
                  <Link href="/courses">
                    <a className={styles.dropDownItem}>Мои курсы</a>
                  </Link>
                </Dropdown.Item>

                <Dropdown.Divider />

                <Dropdown.Item onClick={logout}>
                  <span className={styles.dropDownItem}>Выйти</span>
                </Dropdown.Item>
              </DropdownButton>
            ) : (
              <Link href="/login">
                <a>
                  <Button className={styles.loginBtn}>Войти</Button>
                </a>
              </Link>
            )}
          </div>
        </Container>
      </Navbar>
    </header>
  );
};
