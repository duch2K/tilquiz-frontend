import { FC } from 'react';
import style from './Contacts.module.scss';

export const Contacts: FC = () => {
  return (
    <section className={style.contacts} id="contacts">
      <div className="container">
        <div className="row">
          <div className="col-md-5">
            <h3>Контакты</h3>
            <ul>
              <li>Телефон: +7(778)333-33-36</li>
              <li>Email: oxana@mail.ru</li>
            </ul>
          </div>
        </div>
      </div>
    </section>
  );
};
