import React, { FC } from 'react';
import { Swiper, SwiperSlide } from 'swiper/react';
import { Pagination } from 'swiper';
import { Col, Container, Row } from 'react-bootstrap';

import 'swiper/css/pagination';
import 'swiper/css';
import styles from './Review.module.scss';

export const Reviews: FC = () => {
  return (
    <section className={styles.reviews} id="reviews">
      <Container>
        <Row>
          <Col className={styles.left} xl={6} lg={6} md={12}>
            <div className={styles.leftBlock}>
              <h2>Отзывы от студентов</h2>
              <p>
                Здесь собраны отзывы от разных наших учеников, окончивших наши курсы.
              </p>
            </div>
          </Col>

          <Col className={styles.right} xl={6} lg={6} md={12}>
            <Swiper
              pagination={{ clickable: true }}
              modules={[Pagination]}
              className={styles.swiper}
              centeredSlides={true}
            >
              <SwiperSlide>
                <div className={styles.card}>
                  <p>
                    Очень классные уроки, особенно понравились игры на казахском. Быстро заговорил на родном, казахском и наконец избавился от языкового барьера. Үлкен рахмет, Оксана!
                  </p>
                  <div className={styles.img}>
                    <img src="/images/me.jpg" alt="person" />
                  </div>
                </div>
              </SwiperSlide>

              <SwiperSlide>
                <div className={styles.card}>
                  <p>
                    OOОчень классные уроки, особенно понравились игры на казахском. Быстро заговорил на родном, казахском и наконец избавился от языкового барьера. Үлкен рахмет, Оксана!
                  </p>
                  <div className={styles.img}>
                    <img src="/images/me.jpg" alt="person" />
                  </div>
                </div>
              </SwiperSlide>
            </Swiper>
          </Col>
        </Row>
      </Container>
    </section>
  );
};
