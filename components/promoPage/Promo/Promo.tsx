import React, { FC } from 'react';
import styles from './Promo.module.scss';

export const Promo: FC = () => {
  return (
    <section className={styles.promo} id="promo">
      <div className={styles.promoInner}>
        <h4>Платформа, где можно обучиться казахскому языку</h4>
        <h1>TilQuiz - курсы казахского языка</h1>
        <p>
          C нами вы можете быстро заговорить на казахском, без языковых барьеров, без акцента по специально подготовленной авторской методике!
        </p>
      </div>
    </section>
  );
};

