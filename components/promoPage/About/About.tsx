import { FC } from 'react';
import { Col, Container, Row } from 'react-bootstrap';
import styles from './About.module.scss';

export const About: FC = () =>{
  return (
    <section className={styles.about} id="about">
      <Container>
        <h2>О нас</h2>
        <h3>Кто мы? Почему мы?</h3>
        <Row className={styles.row}>
          <Col xl={6} lg={6} md={12} className={styles.text}>
            <h4>800+</h4>
            <p>
              выпустившихся учеников, свободно владеющие и говорящие на государственном языке.
            </p>
          </Col>
          <Col xl={6} lg={6} md={12} className={styles.img}>
            <img src="/images/about-bg-1.png" />
          </Col>
        </Row>
        <Row className={styles.row}>
          <Col xl={6} lg={6} md={12} className={styles.img}>
            <img src="/images/about-bg-2.png" />
          </Col>
          <Col xl={6} lg={6} md={12} className={styles.text}>
            <h4>Авторская</h4>
            <p>
              методика изучения казахского языка, разработанная последством опыта изучения и преподования 18 лет!
            </p>
          </Col>
        </Row>
        <Row className={styles.row}>
          <Col xl={6} lg={6} md={12} className={styles.text}>
            <h4>Онлайн</h4>
            <p>
              курс, который подойдет для всех желающих, удобно разделен на два потока: для детей и для взрослых. Для каждого потока сделана индивидуальная методика, после прохождения которой уже через 1 месяц вы заговорите на казахском языке!
            </p>
          </Col>
          <Col xl={6} lg={6} md={12} className={styles.img}>
            <img src="/images/about-bg-3.png" />
          </Col>
        </Row>
      </Container>
    </section>
  );
};
