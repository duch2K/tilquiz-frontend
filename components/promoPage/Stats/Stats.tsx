import React, { FC } from 'react';
import { Col, Container, Row } from 'react-bootstrap';
import styles from './Stats.module.scss';

export const Stats: FC = () => {
  return (
    <section className={styles.stats} id="stats">
      <Container fluid>
        <Row>
          <Col className={styles.left} xl={6} lg={6} md={6} sm={12}>
            <div className={styles.img}>
              <img src="/images/measurement.svg" alt="measurement" />
            </div>
            <div className={styles.value}>89%</div>
            <div className={styles.text}>
              Процень окончивших наш курс обучения
            </div>
          </Col>

          <Col className={styles.right} xl={6} lg={6} md={6} sm={12}>
            <div className={styles.img}>
              <img src="/images/people.svg" alt="people" />
            </div>
            <div className={styles.value}>3123</div>
            <div className={styles.text}>
              Общее число обучающихся
            </div>
          </Col>
        </Row>
      </Container>
    </section>
  );
};
