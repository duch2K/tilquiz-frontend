import { FC } from 'react';
import { Accordion, Container } from 'react-bootstrap';
import styles from './FAQ.module.scss';

const faqData = [
  { title: 'Сколько длится взрослый курс?', content: 'Полная программа расчитана на 12 месяцев'},
  { title: 'Сколько длится детский курс?', content: 'Дети учатся на детском курсе до 11 лет, после переходят на общий курс'},
  { title: 'Как рабоать с платформой?', content: 'Алгорит действий: вы смотрите видеоурок, выполняете задания, отправляете мне на проверку. Я проверяю и отправляю свои комментарии к уроку. После "зачета" вам открывается следующий урок.'},
  { title: 'Сейчас цена со скидкой, в следующем месяце поднимется?', content: 'Возможно'}
];

export const FAQ: FC = () => {
  return (
    <section className={styles.faq}>
      <h2 className={styles.faqTitle}>Часто задаваемые вопросы</h2>

      <Container>
        <Accordion className={styles.faqBlock} flush>
          {faqData.map(item => (
            <Accordion.Item
              eventKey={item.title}
              key={item.title}
              style={{ background: '#F5F8FF' }}
            >
              <Accordion.Header>
                { item.title }
              </Accordion.Header>
              <Accordion.Body>
                { item.content }
              </Accordion.Body>
            </Accordion.Item>
          ))}
        </Accordion>
      </Container>
    </section>
  );
};
