import React, { FC } from 'react';
import { Button, Col, Container, Row } from 'react-bootstrap';
import styles from './Subscription.module.scss';

export const Subscription: FC = () => {
  return (
    <section className={styles.sub} id="sub">
      <Container>
        <Row>
          <Col className={styles.left} xl={6} lg={6} md={12}>
            <h3>Запишитесь на бесплатный урок</h3>
            <p>
              Запишитесь на пробный, бесплатный урок, чтобы узнать какой у Вас уровень и подойдут ли наши курсы Вам.
              Оставьте Ваш номер телефона, и мы Вам обязательно перезвоним!
            </p>
          </Col>

          <Col className={styles.right} xl={6} lg={6} md={12}>
            <input type="text" className="form-control" name="email" placeholder="Email" />
            <Button variant="primary" size="lg" type="submit">Сохранить</Button>
          </Col>
        </Row>
      </Container>
    </section>
  );
};

