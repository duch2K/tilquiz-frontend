import { FC } from 'react';
import Link from 'next/link';
import { Button, Col, Container, Row } from 'react-bootstrap';
import cn from 'classnames';
import styles from './Cost.module.scss';

export const Cost: FC = () => {
  return (
    <section className={styles.cost} id="cost">
      <Container>
        <h2>Выберите свою программу обучения</h2>
        <Row className={cn(styles.row, 'align-items-stretch justify-content-center')}>
          <Col xl={4} lg={4} md={6} sm={12}>
            <div className={styles.item}>
              <h3 className="title">Детский онлайн курс</h3>
              <ul>
                <li>️8 занятий в месяц</li>
                <li>Ребенок занимается в любое удобное время</li>
                <li>Красочный и доступный материал с аудированием</li>
                <li>Проверка домашнего задания</li>
                <li>Онлайн-игры для закрепления пройденной темы</li>
                <li>Чат 24/7</li>
                <li>Цена: 10 000 тг</li>
              </ul>
              <div>
                <Link href="/courses">
                  <a>
                    <Button variant="info">
                      Подробнее
                    </Button>
                  </a>
                </Link>
              </div>
            </div>
          </Col>

          <Col xl={4} lg={4} md={6} sm={12}>
            <div className={styles.item}>
              <h3 className="title">Взрослый онлайн курс</h3>
              <ul>
                <li>12 занятий в месяц</li>
                <li>️Обучение проходит в любое удобное для вас время</li>
                <li>Иллюстрация заданий и аудиодорожки новых слов на двух языках</li>
                <li>Проверка домашнего задания</li>
                <li>Онлайн-игры для закрепления пройденной темы</li>
                <li>Чат 24/7</li>
                <li>Цена: 13 000 тг</li>
              </ul>
              <div>
                <Link href="/courses">
                  <a>
                    <Button variant="info">
                      Подробнее
                    </Button>
                  </a>
                </Link>
              </div>
            </div>
          </Col>

          <Col xl={4} lg={4} md={6} sm={12}>
            <div className={styles.item}>
              <h3 className="title">Разговорный клуб г. Алматы</h3>
              <ul>
                <li>️Вторник и четверг 19:30-20:30 продвинутый уровень для взрослых</li>
                <li>Среда 19:30-20:30 средний уровень для взрослых</li>
                <li>Четверг 19:30-20:30 базовый уровень для взрослых</li>
                <li>Суббота 15:00-16:00 детский разговорный клуб 16:00-17:00 начальный уровень для взрослых</li>
                <li>Цена: 16 000 тг</li>
              </ul>
              <div>
                <Link href="/courses">
                  <a>
                    <Button variant="info">
                      Подробнее
                    </Button>
                  </a>
                </Link>
              </div>
            </div>
          </Col>
        </Row>
      </Container>
    </section>
  );
};
