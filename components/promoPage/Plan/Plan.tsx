import { FC } from 'react';
import cn from 'classnames';
import styles from './Plan.module.scss';
import { Button } from 'react-bootstrap';

export const Plan: FC = () => {
  return (
    <section className={styles.plan} id="plan">
      <div className="container">
        <div className="row">
          <div className={cn(styles.left, 'col-md-8 col-lg-4 col-xl-4')}>
            <div className={styles.planBlock}>
              <h4>Полно-адаптированный сайт для комфортного обучения</h4>
              <h2>Введение</h2>
              <p>
                Для удобного прохождения обучения мы подготовили вам удобный вид коммуникации - сделали комфортабельный, читабельный сайтс интуитивно понятным интерфесом. После прохождения регистрации вы можете смотреть видеоуроки, проходить тесты, играть в обучающие игры и следить за вашими успехами.
              </p>
              <div className={cn(styles.planBtns, 'form-inline align-items-stretch')}>
                <Button variant="primary">Смотреть видео</Button>
              </div>
            </div>
          </div>

          <div className={cn(styles.right, 'col-md-8')} />
        </div>
      </div>
    </section>
  );
};
