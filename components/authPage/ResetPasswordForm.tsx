import axios from 'axios';
import React, { ChangeEventHandler, FormEventHandler, useState } from 'react';
import { Button, Card } from 'react-bootstrap';
import Link from 'next/link';
import { useRouter } from 'next/router';
import { apiUrl } from '@/config';

export const ResetPasswordForm = () => {
  const [password, setPassword] = useState<string>('');
  const [confirmPassword, setConfirmPassword] = useState<string>('');
  const [error, setError] = useState<string | null>(null);
  const router = useRouter();
  const { token, email } = router.query;

  const handlePasswordChange: ChangeEventHandler<HTMLInputElement> = (e) => {
    setPassword(e.target.value);
  };
  const handleConfirmPasswordChange: ChangeEventHandler<HTMLInputElement> = (e) => {
    setConfirmPassword(e.target.value);
  };

  const handleResetPassword: FormEventHandler = async (e) => {
    try {
      e.preventDefault();

      if (password !== confirmPassword) {
        setError('Пароли не совпадают!');
        return;
      }

      const res = await axios.post(`${apiUrl}/auth/confirm`, {
        email,
        token
      });

      if (!res.data) {
        setError('Что то пошло не так? Попробуйте позже.');
      }

      await axios.post(`${apiUrl}/auth/newPassword`, {
        email,
        password
      });

      await router.push('/login');

    } catch (err) {
      setError('Что то пошло не так.');
      console.log(err);
    }
  };

  return (
    <Card className="p-4">
      <form className="form w-100 pt-3 mb-3" onSubmit={handleResetPassword}>
        <h5 className="mb-4">Восстановление пароля</h5>

        {error && (<p style={{color: 'red'}}>{error.length ? error : 'Что то пошло не так.'}</p>)}
        <div className="mb-4">
          <input
            type="password"
            className="form-control form-control-sm"
            placeholder="Новый пароль"
            onChange={handlePasswordChange}
            value={password}
          />
        </div>
        <div className="mb-4">
          <input
            type="password"
            className="form-control form-control-sm"
            placeholder="Повторите новый пароль"
            onChange={handleConfirmPasswordChange}
            value={confirmPassword}
          />
        </div>

        <Button variant="primary" type="submit">Отправить</Button>
      </form>
      <Link href="/login">
        <a className="small">Войти</a>
      </Link>
    </Card>
  );
};
