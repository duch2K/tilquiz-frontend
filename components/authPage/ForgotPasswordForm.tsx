import React, { ChangeEventHandler, FormEventHandler, useState } from 'react';
import { useRouter } from 'next/router';
import Link from 'next/link';
import { Button, Card } from 'react-bootstrap';
import axios from 'axios';
import { apiUrl } from '@/config';

export const ForgotPasswordForm = () => {
  const [email, setEmail] = useState<string>('');
  const [error, setError] = useState<boolean>(false);
  const router = useRouter();

  const handleEmailChange: ChangeEventHandler<HTMLInputElement> = (e) => {
    setEmail(e.target.value);
  };

  const handleRegister: FormEventHandler = async (e) => {
    try {
      e.preventDefault();

      const res = await axios.post(`${apiUrl}/auth/reset`, {
        email
      });

      router.push('/check-email');
      console.log('reset result', res);

    } catch (err) {
      setError(true);
      console.log(err);
    }
  };

  return (
    <Card className="p-4">
      <form className="form w-100 pt-3 mb-3" onSubmit={handleRegister}>
        <h5 className="mb-4">Восстановление пароля</h5>

        {error && (<h2 style={{color: 'red'}}>Password reset Error!</h2>)}
        <div className="mb-4">
          <input
            type="email"
            className="form-control form-control-sm"
            placeholder="Ваш email"
            onChange={handleEmailChange}
            value={email}
          />
        </div>

        <Button variant="primary" type="submit">Отправить</Button>
      </form>
      <Link href="/login">
        <a className="small">Войти</a>
      </Link>
    </Card>
  );
};
