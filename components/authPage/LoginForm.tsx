import React, { ChangeEventHandler, FormEventHandler, useState } from 'react';
import { useRouter } from 'next/router';
import Link from 'next/link';
import { Button, Card } from 'react-bootstrap';
import axios from 'axios';
import { setCookie } from 'nookies';
import { apiUrl } from '@/config';

export const LoginForm = () => {
  const [email, setEmail] = useState<string>('');
  const [pass, setPass] = useState<string>('');
  const [error, setError] = useState<boolean>(false);

  const router = useRouter();

  const handleEmailChange: ChangeEventHandler<HTMLInputElement> = (e) => {
    setEmail(e.target.value);
  };
  const handlePassChange: ChangeEventHandler<HTMLInputElement> = (e) => {
    setPass(e.target.value);
  };

  const handleLogin: FormEventHandler = async (e) => {
    try {
      e.preventDefault();

      const { data } = await axios.post(`${apiUrl}/auth/login`, {
        email,
        password: pass
      });

      setCookie(null, 'auth', JSON.stringify(data), {
        maxAge: 30 * 24 * 60 * 60,
        path: '/'
      });

      router.push('/courses');
    } catch (err) {
      setError(true);
      console.log(err);
    }
  };

  return (
    <Card className="p-4">
      <form className="form w-100 mb-3" onSubmit={handleLogin}>
        <h4 className="mb-4">Войти</h4>

        {error && (<p style={{color: 'red'}}>Неправильный email или пароль!</p>)}
        <div className="mb-3">
          <input
            type="text"
            className="form-control form-control-sm"
            placeholder="Email"
            onChange={handleEmailChange}
            value={email}
          />
        </div>
        <div className="mb-3">
          <input
            type="password"
            className="form-control form-control-sm mb-1"
            placeholder="Пароль"
            onChange={handlePassChange}
            value={pass}
          />
          <Link href="/forgot-password">
            <a className="small d-block text-end">Забыли пароль?</a>
          </Link>
        </div>

        <Button as="div" variant="primary" type="submit" onClick={handleLogin}>Войти</Button>
      </form>
      <Link href="/register">
        <a className="small">Зарегистрироваться</a>
      </Link>
    </Card>
  );
};
