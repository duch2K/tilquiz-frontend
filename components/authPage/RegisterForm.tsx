import React, { ChangeEventHandler, FormEventHandler, useState } from 'react';
import { useRouter } from 'next/router';
import Link from 'next/link';
import axios from 'axios';
import { Button, Card } from 'react-bootstrap';
import { apiUrl } from '@/config';

export const RegisterForm = () => {
  const [name, setName] = useState<string>('');
  const [surname, setSurname] = useState<string>('');
  const [email, setEmail] = useState<string>('');
  const [pass, setPass] = useState<string>('');
  const [error, setError] = useState<boolean>(false);

  const router = useRouter();

  const handleNameChange: ChangeEventHandler<HTMLInputElement> = (e) => {
    setName(e.target.value);
  };
  const handleSurnameChange: ChangeEventHandler<HTMLInputElement> = (e) => {
    setSurname(e.target.value);
  };
  const handleEmailChange: ChangeEventHandler<HTMLInputElement> = (e) => {
    setEmail(e.target.value);
  };
  const handlePassChange: ChangeEventHandler<HTMLInputElement> = (e) => {
    setPass(e.target.value);
  };

  const handleRegister: FormEventHandler = async (e) => {
    try {
      e.preventDefault();

      const res = await axios.post(`${apiUrl}/auth/registration`, {
        name,
        surname,
        email,
        password: pass
      });

      await router.push('/courses');
    } catch (err) {
      setError(true);
      console.log(err);
    }
  };

  return (
    <Card className="p-4">
      <form className="form w-100 mb-3" onSubmit={handleRegister}>
        <h4 className="mb-4">Регистрация</h4>

        {error && (<p style={{color: 'red'}}>Ошибка регистрации!</p>)}
        <div className="mb-4">
          <input
            type="text"
            className="form-control form-control-sm"
            placeholder="Имя"
            onChange={handleNameChange}
            value={name}
          />
        </div>
        <div className="mb-4">
          <input
            type="text"
            className="form-control form-control-sm"
            placeholder="Фамилия"
            onChange={handleSurnameChange}
            value={surname}
          />
        </div>
        <div className="mb-4">
          <input
            type="email"
            className="form-control form-control-sm"
            placeholder="Email"
            onChange={handleEmailChange}
            value={email}
          />
        </div>
        <div className="mb-4">
          <input
            type="password"
            className="form-control form-control-sm"
            placeholder="Пароль"
            onChange={handlePassChange}
            value={pass}
          />
        </div>

        <Button variant="primary" type="submit">Зарегистрироваться</Button>
      </form>
      <Link href="/login">
        <a className="small">Войти</a>
      </Link>
    </Card>
  );
};
