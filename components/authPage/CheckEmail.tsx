import { Card } from 'react-bootstrap';
import React from 'react';

export const CheckEmail = () => {
  return (
    <Card className="p-4">
      <h3>Провертье почту</h3>
      <p>Мы отправили ссылку для смены пароля на вашу почту.</p>
    </Card>
  );
};