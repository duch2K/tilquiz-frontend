import { FC, useEffect, useState } from 'react';
import { Button, Container, Form, Modal } from 'react-bootstrap';
import styles from './Test.module.scss';
import { quiz } from '@/components/Test/quiz';
import axios from 'axios';
import { apiUrl } from '@/config';
import { useRouter } from 'next/router';
import { parseCookies } from 'nookies';
import Link from 'next/link';

interface Props {
  items: any[];
}

export const Test: FC<Props> = ({ items }) => {
  const router = useRouter();
  const cookies = parseCookies(null);
  const newItems = router.pathname.includes('trial-test') ? items : items.slice(5, 10);
  console.log('YO', newItems)
  const [ans, setAns] = useState<any[]>(newItems.map(item => ({
    id: item.id,
    right: false
  })));
  const [score, setScore] = useState<any>({right: 0, value: 0});
  // const questions: any[] = newItems?.length > 0 ? items : quiz;

  const [modalVisible, setModalVisible] = useState<boolean>(false);

  useEffect(() => {
    const ansList = newItems.map(item => ({ id: item.id, right: null }));
    setAns(ansList);
  }, []);

  const handleClose = () => setModalVisible(false);

  const submit = async () => {
    const testRes = ans.filter(item => item.right).length;
    setScore({
      right: testRes,
      value: (testRes / ans.length * 100).toFixed(2)
    })
    setModalVisible(true)
  };

  const changeAns = (id: number | string, ansItem: any) => {
    setAns(() => {
      const newAns = ans.map(item => item)
      const idx = ans.map(item => item.id).indexOf(id);

      newAns[idx].right = ansItem.right

      return newAns;
    });
  };

  return (
    <section className={styles.testQuiz}>
      <Container className={styles.container}>
        <h2>
          Тест по теме
          Урок №2. Дауыссыз дыбыстар
        </h2>
        <div className={styles.items}>
          {newItems.map((item: any, idx: number) => (
            <div className={styles.item} key={item.id}>
              <h4>{`${idx + 1} ${item.text}`}</h4>
              <ul>
                {item.answers.map((ansItem: any) => (
                  <li key={item.id + ansItem.text}>
                    <Form.Check
                      type="radio"
                      name={String(item.text)}
                      label={ansItem.text}
                      onChange={() => changeAns(item.id, ansItem)}
                    />
                  </li>
                ))}
              </ul>
            </div>
          ))}
        </div>
        <Button
          className={styles.btn}
          variant="info"
          onClick={submit}
        >
          Сдать
        </Button>
      </Container>

      <Modal show={modalVisible} onHide={handleClose}>
        <Modal.Header className={styles.modalTitle} closeButton>
          <Modal.Title>Результат теста</Modal.Title>
        </Modal.Header>
        <Modal.Body>
          Баллов: <b>{score.value}%</b>
          <br/>
          Правильных: <b>{score.right}/{ans.length}</b>
          <br/>
          Ваш уровень: <b>{
            score.right < 14 ? '"Начальный"' : '"Продвинутый"'
          }</b>
        </Modal.Body>
      </Modal>
    </section>
  );
};
