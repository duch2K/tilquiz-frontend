export const quiz = [
  {
    id: 1,
    question: 'Ұяң дауыссыздарды табыңыз.',
    answers: [
      {
        id: 1,
        title: 'Qwerty',
        correct: false
      },
      {
        id: 2,
        title: 'Asdfg',
        correct: false
      },
      {
        id: 3,
        title: 'Zxcvb',
        correct: false
      },
      {
        id: 4,
        title: 'Jklnm',
        correct: false
      }
    ]
  },
  {
    id: 2,
    question: 'Қатаң дауыссыздарды табыңыз.',
    answers: [
      {
        id: 5,
        title: 'Qwerty',
        correct: false
      },
      {
        id: 6,
        title: 'Asdfg',
        correct: false
      },
      {
        id: 7,
        title: 'Zxcvb',
        correct: false
      },
      {
        id: 8,
        title: 'Jklnm',
        correct: false
      }
    ]
  },
  {
    id: 3,
    question: 'Үндi дауыссыздарды табыңыз.',
    answers: [
      {
        id: 9,
        title: 'Qwerty',
        correct: false
      },
      {
        id: 10,
        title: 'Asdfg',
        correct: false
      },
      {
        id: 11,
        title: 'Zxcvb',
        correct: false
      },
      {
        id: 12,
        title: 'Jklnm',
        correct: false
      }
    ]
  }
]
