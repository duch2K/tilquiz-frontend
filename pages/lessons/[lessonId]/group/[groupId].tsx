import React, { useEffect, useState } from 'react';
import { useRouter } from 'next/router';
import { slide as Menu } from 'react-burger-menu';
import { Col, Container, Row } from 'react-bootstrap';
import axios from 'axios';
import { Footer, Header } from '@/components';
import { LessonComponent, Sidebar, LessonHead } from '@/components/lessonsPage';
import { apiUrl } from '@/config';
import styles from '@/pages/lessons/lesson.module.scss';

const Lesson = () => {
  const router = useRouter();
  const [lessons, setLessons] = useState<any[]>([]);
  const [current, setCurrent] = useState<any>(null);
  const [isOpen, setIsOpen] = useState<boolean>(false);

  useEffect(() => {
    fetchLessons().then(res => {
      const lessonId = Number(router.query.lessonId);

      if (!isNaN(lessonId) && lessonId) {
        setLessons(res);
        setCurrent(res.find((item: any) => item.id == lessonId));
      }
    });
  }, [router.isReady]);

  const fetchLessons = async () => {
    try {
      const res = await axios.get(`${apiUrl}/lessons`);
      return res.data;
    } catch (err) {
      setLessons([]);
      setCurrent(null);
    }
  };

  return (
    <div id="lesson">
      {/*@ts-ignore*/}
      <Menu
        isOpen={isOpen}
        onClose={() => setIsOpen(false)}
        customCrossIcon={<img src="/images/close-x.webp" />}
        customBurgerIcon={false}
        menuClassName={styles.menu}
        outerContainerId={'lesson'}
      >
        <Sidebar
          lessons={lessons}
          currLesson={current}
          setLesson={setCurrent}
          setIsOpen={setIsOpen}
        />
      </Menu>

      <Header />

      <div className="lesson-page" id="lesson-page">
        <LessonHead setIsOpen={setIsOpen} />

        <Container>
          <Row>
            <Col lg={3} className={styles.sidebar}>
              <Sidebar lessons={lessons} currLesson={current} setLesson={setCurrent} />
            </Col>
            <Col lg={9} md={12}>
              <LessonComponent lessons={lessons} current={current} />
            </Col>
          </Row>
        </Container>
      </div>

      <Footer />
    </div>
  );
};

export default Lesson;
