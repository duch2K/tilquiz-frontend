import { useEffect, useState } from 'react';
import { useRouter } from 'next/router';
import { NextPage } from 'next';
import Head from 'next/head';
import axios from 'axios';
import { Test } from '@/components/Test/Test';
import { Header } from '@/components';
import { apiUrl } from '@/config';
import { trialTest } from '@/trial-test';

const TrialTestPage: NextPage = () => {
  const router = useRouter();
  const [current, setCurrent] = useState<any>(null);

  // console.log('current', current)

  useEffect(() => {
    fetchLessons();
  }, [router.isReady]);

  const fetchLessons = async () => {
    try {
      // const res = await axios.get(`${apiUrl}/lessons/` + router.query.lessonId);
      const res = await axios.get(`${apiUrl}/questions`);

      setCurrent(res.data);

    } catch (err) {
      setCurrent(null);
    }
  };

  return (
    <>
      <Head>
        <title>Тест</title>
      </Head>

      <Header />

      {current && <Test items={trialTest}/>}
    </>
  );
};

export default TrialTestPage;
