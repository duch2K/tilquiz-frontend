import { AppProps } from 'next/app';
import Head from 'next/head';
import 'bootstrap/dist/css/bootstrap.min.css';
import 'swiper/css/bundle';
import '../styles/main.scss';

function MyApp({ Component, pageProps }: AppProps) {
  return (
    <>
      <Head>
        <meta charSet="utf-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1" />
        <title>TilQuiz</title>
      </Head>

      {/*@ts-ignore*/}
      <Component {...pageProps} />
    </>
  );
}

export default MyApp;
