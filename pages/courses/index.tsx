import { useEffect, useState } from 'react';
import Link from 'next/link';
import { useRouter } from 'next/router';
import { Button, Col, Container, Modal, Row } from 'react-bootstrap';
import { parseCookies } from 'nookies';
import { Footer, Header } from '@/components';
import styles from './courses.module.scss';
import { apiUrl, token } from '@/config';
import axios from 'axios';

const items = [
  {
    id: 1,
    title: 'Бесплатный урок',
    list: [
      '1 занятие',
      'Занятие можно пройти в любое удобное для вас время',
      'Иллюстрация заданий и аудиодорожки новых слов на двух языках',
      'Тест для проверки уровня знания',
      'Чат 24'
    ]
  },
  {
    id: 2,
    title: 'Тема 1. Начальный уровень',
    list: [
      '12 занятие',
      'Обучение проходит в любое удобное для вас время',
      'Иллюстрация заданий и аудиодорожки новых слов на двух языках',
      'Проверка домашнего задания',
      'Тест для проверки уровня знания',
      'Тема и уроки начального уровня казахского языка',
      'Чат 24'
    ]
  },
  {
    id: 3,
    title: 'Тема 2. Продвинутый уровень',
    list: [
      '12 занятие',
      'Обучение проходит в любое удобное для вас время',
      'Иллюстрация заданий и аудиодорожки новых слов на двух языках',
      'Проверка домашнего задания',
      'Тема и уроки продвинутого уровня казахского языка',
      'Чат 24'
    ]
  }
];

const Courses = () => {
  const router = useRouter();
  const cookies = parseCookies(null);
  const [user, setUser] = useState<any>(null);
  const [sortedItems, setSortedItems] = useState<any[]>([]);

  useEffect(() => {
    if (cookies?.auth && JSON.parse(cookies.auth)?.id)
      getUser()
    else
      setSortedItems([items[0]])

  }, [])

  const goToCourse = (id: number | string) => {
    if (cookies.auth && JSON.parse(cookies.auth)?.id) {
      router.push('/lessons/1/group/' + id);
      return;
    }
  };

  const getUser = async () => {
    const res: any = await axios.get(apiUrl + '/users', {
      headers: {
        Authorization: 'Bearer ' + token
      }
    });

    const userData = res.data.find((item: any) => item.id == JSON.parse(cookies.auth).id);
    setUser(userData);

    const sorted = userData?.roles?.length ? items : [items[0]]
    setSortedItems(sorted);
  };

  return (
    <>
      <Header />

      <Container className={styles.container}>
        <h3 className={styles.title}>Доступные курсы</h3>
        <Row style={{justifyContent: 'center'}}>
          {user?.roles && sortedItems ? sortedItems.map(item => (
            <Col md={3} key={item.title}>
              <div className={styles.item}>
                <h5>{item.title}</h5>
                <ul>
                  {item.list.map(l => (
                    <li key={l}>{l}</li>
                  ))}
                </ul>
                <div>
                  <Button variant="info" onClick={() => goToCourse(item.id)}>
                    Перейти
                  </Button>
                </div>
              </div>
            </Col>
          )) : (
            <Col md={4}>
              <div style={{display: 'flex', flexDirection: 'column', alignItems: 'center'}}>
                <p style={{textAlign: 'center'}}>
                  Для подробной информации о курсе нужно зарегестрироваться на сайте.
                  Регистрируйтесь и открывайте для себя новые познания в казахском языке!
                </p>
                <div style={{display: 'flex', gap: 30}}>
                  <Link href="/login">
                    <a>
                      <Button variant="info">Войти</Button>
                    </a>
                  </Link>
                  <Link href="/register">
                    <a>
                      <Button variant="info">Зарегистрироваться</Button>
                    </a>
                  </Link>
                </div>
              </div>
            </Col>
          )}
        </Row>
      </Container>
      <Footer />
    </>
  );
};

export default Courses;
