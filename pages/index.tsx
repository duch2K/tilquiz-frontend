import { NextPage } from 'next';
import Head from 'next/head';
import { Footer, Header } from '@/components';
import {
  Subscription,
  Plan,
  Promo,
  Reviews,
  Stats,
  Cost,
  Contacts,
  FAQ,
  About
} from '@/components/promoPage';

const Home: NextPage = () => {
  return (
    <>
      <Head>
        <title>Главная</title>
      </Head>

      <Header />

      <Promo />

      <Plan />

      <About />

      <Cost />

      <FAQ />

      <Stats />

      <Reviews />

      <Subscription />

      <Contacts />

      <Footer />
    </>
  );
};

export default Home;
