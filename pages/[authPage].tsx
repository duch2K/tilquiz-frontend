import { useRouter } from 'next/router';
import {
  ResetPasswordForm,
  LoginForm,
  RegisterForm,
  ForgotPasswordForm
} from '@/components/authPage';
import { CheckEmail } from '@/components/authPage/CheckEmail';
import { Header } from '@/components';

const routes = [
  {
    slug: 'login',
    component: <LoginForm />
  },
  {
    slug: 'register',
    component: <RegisterForm />
  },
  {
    slug: 'forgot-password',
    component: <ForgotPasswordForm />
  },
  {
    slug: 'reset-password',
    component: <ResetPasswordForm />
  },
  {
    slug: 'check-email',
    component: <CheckEmail />
  }
];

const AuthPage = () => {
  const router = useRouter();

  const page = routes.find(item => item.slug === router.query.authPage);

  return (
    <>
      <Header />

      <div className="login text-center">
        <div className="container py-3">
          <div className="m-auto" style={{width: 430}}>
            { page && page.component }
          </div>
        </div>
      </div>
    </>
  );
};

export default AuthPage;
